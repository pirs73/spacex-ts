import { getDataFromTree } from '@apollo/client/react/ssr';
import withApollo from '../hoc/withApollo';

const Home = () => {
  return (
    <div className="container testPostCss">
      <h2>Launches</h2>
      <p>test</p>
      <span className="testPostCss--nested">green</span>
    </div>
  );
};

export default withApollo(Home, { getDataFromTree });
